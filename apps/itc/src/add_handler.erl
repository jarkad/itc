-module(add_handler).

-export([init/2,
         allowed_methods/2,
         content_types_accepted/2,
         from_etf/2,
         from_json/2]).

-record(state, {}).

init(Req, []) -> {cowboy_rest, Req, #state{}}.

allowed_methods(Req, State) ->
    {[<<"OPTIONS">>, <<"POST">>], Req, State}.

content_types_accepted(Req, State) ->
    {[{<<"application/json">>, from_json},
      {<<"application/x-etf">>, from_etf}],
     Req,
     State}.

from_etf(Req,State)->
  {true,Req,State}.

from_json(Req,State)->
  {true,Req,State}.
