-module(constraints).

-compile(export_all).

timestamp(forward, Value) ->
    try {ok, itc_clock:binary_to_timestamp(Value)} catch
        _:_ -> {error, invalid_timestamp}
    end;
timestamp(reverse, Value) ->
    try {ok, itc_clock:timestamp_to_binary(Value)} catch
        _:_ -> {error, invalid_timestamp}
    end;
timestamp(format_error, {invalid_timestamp, Value}) ->
    io_lib:format("Invalid timestamp: ~tp~n", [Value]).

base64(forward, Value) ->
    try {ok, base64:decode(Value)} catch
        _:_ -> {error, invalid_base64}
    end;
base64(reverse, Value) ->
    try {ok, base64:encode(Value)} catch
        _:_ -> {error, invalid_base64}
    end;
base64(format_error, {invalid_base64, Value}) ->
    io_lib:format("Invalid base64: ~tp~n", [Value]).
