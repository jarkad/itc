%%% Distributed append-only causally ordered event log.
%%% User interface:
%%% now()
%%%     Returns combined timestamp of all events. Used
%%%     in combination with since/1 and add/1 to
%%%     synchronize databases.
%%% all()
%%%     Returns all events. Used to bootstrap a new
%%%     database.
%%% since(Timestamp)
%%%     Returns all events that happened after (or
%%%     concurrently to) a given timestamp.
%%% subscribe(Fun, Timestamp)
%%%     Subscribe to all events that happened after the
%%%     Timestamp. Each event will be sent to the
%%%     caller:
%%%
%%%     init(Args) ->
%%%         ...,
%%%         % Subscribe to events.
%%%         subscribe(fun(Event) -> {event, Event} end, events_model:now()),
%%%         ....
%%%
%%%     handle_cast({event, Event}, State) ->
%%%         % Handle the event.
%%%         ...,
%%%         {noreply, State}.
%%% new(Node_Id)
%%%     Creates a timestamp. Used to generate new
%%%     events.
%%% add(Events)
%%%     Adds events generated externally to the
%%%     database. Used to apply diffs. NOTE: existing
%%%     events are overwritten.

-module(events_model).

-behaviour(gen_server).

-export([start_link/1, start_link/2]).

-export([now/1,
         all/1,
         since/2,
         subscribe/2,
         new/3,
         add/2]).

-export([init/1,
         handle_call/3,
         handle_cast/2,
         terminate/2]).

-record(state, {now}).

start_link(File) ->
    gen_server:start_link(?MODULE, [File], []).

start_link(Name, File) ->
    gen_server:start_link(Name, ?MODULE, File, []).

now(Name) -> gen_server:call(Name, now).

all(Name) -> gen_server:call(Name, all).

since(Name, Since) ->
    gen_server:call(Name, {since, Since}).

subscribe(Name, Since) ->
    gen_server:call(Name, {subscribe, Since}).

new(Name, Id, Payload) ->
    gen_server:call(Name, {new, Id, Payload}).

add(Name, Events) ->
    gen_server:cast(Name, {add, Events}).

init(File) ->
    {ok, _} = dets:open_file(db,
                             [{type, set}, {file, File}]),
    Now = dets:foldl(fun ({T, _}, N) ->
                             itc_clock:join_event(N, T)
                     end,
                     itc_clock:new_event(),
                     db),
    {ok, #state{now = Now}}.

handle_call(now, _From, #state{now = Now} = State) ->
    {reply, Now, State};
handle_call(all, _From, State) ->
    Events = dets:traverse(db,
                           fun (X) -> {continue, X} end),
    {reply, Events, State};
handle_call({since, Since}, _From, State) ->
    Filter = fun ({Timestamp, _} = Event) ->
                     case itc_clock:compare(Timestamp, Since) of
                         less -> continue;
                         equal -> continue;
                         _ -> {continue, Event}
                     end
             end,
    Events = dets:traverse(db, Filter),
    {reply, Events, State};
handle_call({new, Id, Payload}, _From, State) ->
    Event = itc_clock:event(Id, State#state.now),
    ok = dets:insert(db, [{Event, Payload}]),
    {reply, Event, State#state{now = Event}}.

handle_cast({add, Events}, State0) ->
    Now = lists:foldl(fun ({T, _}, N) ->
                              itc_clock:join_event(N, T)
                      end,
                      State0#state.now,
                      Events),
    State = State0#state{now = Now},
    ok = dets:insert(db, Events),
    {noreply, State}.

terminate(_Reason, _State) -> ok = dets:close(db).
