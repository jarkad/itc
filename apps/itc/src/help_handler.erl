-module(help_handler).

-export([init/2,
         content_types_provided/2,
         to_etf/2,
         to_json/2,
         to_text/2]).

-record(state, {}).

init(Req, []) -> {cowboy_rest, Req, #state{}}.

content_types_provided(Req, State) ->
    {[{<<"text/plain">>, to_text},
      {<<"application/json">>, to_json},
      {<<"application/x-etf">>, to_etf}],
     Req,
     State}.

help() ->
    [{<<"GET">>, "/", "Show help"},
     {<<"GET">>, "/now", "Get current timestamp"},
     {<<"GET">>, "/all", "Get all events"},
     {<<"GET">>,
      "/since/<timestamp>",
      "Get all events since the timestamp"},
     {<<"POST">>, "/add", "Add new events"}].

to_etf(Req, State) ->
    {term_to_iovec(help()), Req, State}.

to_text(Req, State) ->
    Help = lists:map(fun ({Method, Path, Description}) ->
                             [Method, " ", Path, " ", Description, "\n"]
                     end,
                     help()),
    {Help, Req, State}.

to_json(Req, State) ->
    {jsx:encode(lists:map(fun ({M, P, D}) ->
                                  #{method => M, path => list_to_binary(P),
                                    desc => list_to_binary(D)}
                          end,
                          help())),
     Req,
     State}.
