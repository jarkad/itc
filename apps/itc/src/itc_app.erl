%%%-------------------------------------------------------------------
%% @doc itc public API
%% @end
%%%-------------------------------------------------------------------

-module(itc_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    Dispatch = cowboy_router:compile([{'_',
                                       [{"/", help_handler, []},
                                        {"/now", now_handler, []},
                                        {"/all", all_handler, []},
                                        {"/since/:since",
                                         [{since,
                                           [fun constraints:base64/2,
                                            fun constraints:timestamp/2]}],
                                         since_handler,
                                         []},
                                        {"/add", add_handler, []}]}]),
    {ok, _} = cowboy:start_clear(http,
                                 [{port, 5000}],
                                 #{env => #{dispatch => Dispatch}}),
    itc_sup:start_link().

stop(_State) -> ok = cowboy:stop_listener(http).

%% internal functions
