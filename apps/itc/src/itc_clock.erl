-module(itc_clock).

-export([new_id/0,
         new_event/0,
         compare/2,
         fork/1,
         join_id/2,
         join_event/2,
         event/2,
         timestamp_to_binary/1,
         binary_to_timestamp/1]).

new_id() -> 1.

new_event() -> 0.

fork(1) -> {{1, 0}, {0, 1}};
fork(0) -> {0, 0};
fork({0, I}) ->
    {I1, I2} = fork(I),
    {{0, I1}, {0, I2}};
fork({I, 0}) ->
    {I1, I2} = fork(I),
    {{I1, 0}, {I2, 0}};
fork({I1, I2}) -> {{I1, 0}, {0, I2}}.

inorm({I, I}) -> I;
inorm(I) -> I.

join_id(0, I) -> I;
join_id(I, 0) -> I;
join_id({L1, R1}, {L2, R2}) ->
    inorm({join_id(L1, L2), join_id(R1, R2)}).

lift({N, L, R}, M) -> {N + M, L, R};
lift(N, M) -> N + M.

emin(N) when is_integer(N) -> N;
emin({N, L, R}) -> N + min(emin(L), emin(R)).

emax(N) when is_integer(N) -> N;
emax({N, L, R}) -> N + max(emax(L), emax(R)).

enorm(N) when is_integer(N) -> N;
enorm({N, M, M}) when is_integer(M) -> N + M;
enorm({N, L, R}) ->
    M = min(emin(L), emin(R)),
    {N + M, lift(L, -M), lift(R, -M)}.

join_event(N1, N2)
    when is_integer(N1), is_integer(N2) ->
    max(N1, N2);
join_event(N1, {N2, E1, E2}) when is_integer(N1) ->
    join_event({N1, 0, 0}, {N2, E1, E2});
join_event({N1, E1, E2}, N2) when is_integer(N2) ->
    join_event({N1, E1, E2}, {N2, 0, 0});
join_event({N1, L1, R1}, {N2, L2, R2}) when N1 > N2 ->
    join_event({N2, L2, R2}, {N1, L1, R1});
join_event({N1, L1, R1}, {N2, L2, R2}) ->
    enorm({N1,
           join_event(L1, lift(L2, N2 - N1)),
           join_event(R1, lift(R2, N2 - N1))}).

fill(0, E) -> E;
fill(1, E) -> emax(E);
fill(_, E) when is_integer(E) -> E;
fill({1, I}, {N, L, R}) ->
    R1 = fill(I, R),
    L1 = max(emax(L), emax(R1)),
    enorm({N, L1, R1});
fill({I, 1}, {N, L, R}) ->
    L1 = fill(I, L),
    R1 = max(emax(R), emax(L1)),
    enorm({N, L1, R1});
fill({IL, IR}, {N, L, R}) ->
    enorm({N, fill(IL, L), fill(IR, R)}).

grow(1, N) when is_integer(N) -> {N + 1, 0};
grow(I, N) when is_integer(N) ->
    {E, C} = grow(I, {N, 0, 0}),
    {E, C + 1000000};
grow({0, I}, {N, L, R}) ->
    {R1, C} = grow(I, R),
    {{N, L, R1}, C + 1};
grow({I, 0}, {N, L, R}) ->
    {L1, C} = grow(I, L),
    {{N, L1, R}, C + 1};
grow({IL, IR}, {N, L, R}) ->
    {L1, CL} = grow(IL, L),
    {R1, CR} = grow(IR, R),
    case CL < CR of
        true -> {{N, L1, R}, CL + 1};
        false -> {{N, L, R1}, CR + 1}
    end.

event(I, E) ->
    FE = fill(I, E),
    case FE == E of
        false -> FE;
        true ->
            {GE, _} = grow(I, E),
            GE
    end.

leq(N1, N2) when is_integer(N1), is_integer(N2) ->
    N1 =< N2;
leq(N1, {N2, _, _}) when is_integer(N1) -> N1 =< N2;
leq({N1, L1, R1}, N2) when is_integer(N2) ->
    N1 =< N2 andalso
        leq(lift(L1, N1), N2) andalso leq(lift(R1, N1), N2);
leq({N1, L1, R1}, {N2, L2, R2}) ->
    N1 =< N2 andalso
        leq(lift(L1, N1), lift(L2, N2)) andalso
            leq(lift(R1, N1), lift(R2, N2)).

compare(E1, E2) ->
    case {leq(E1, E2), leq(E2, E1)} of
        {true, true} -> equal;
        {true, false} -> less;
        {false, true} -> greater;
        {false, false} -> concurrent
    end.

timestamp_to_list({0, 0, E}) ->
    [<<0:1, 0:2>>, timestamp_to_list(E)];
timestamp_to_list({0, E, 0}) ->
    [<<0:1, 1:2>>, timestamp_to_list(E)];
timestamp_to_list({0, E1, E2}) ->
    [<<0:1, 2:2>>,
     timestamp_to_list(E1),
     timestamp_to_list(E2)];
timestamp_to_list({N, 0, E}) ->
    [<<0:1, 3:2, 0:1, 0:1>>,
     timestamp_to_list(N),
     timestamp_to_list(E)];
timestamp_to_list({N, E, 0}) ->
    [<<0:1, 3:2, 0:1, 1:1>>,
     timestamp_to_list(N),
     timestamp_to_list(E)];
timestamp_to_list({N, E1, E2}) ->
    [<<0:1, 3:2, 1:1>>,
     timestamp_to_list(N),
     timestamp_to_list(E1),
     timestamp_to_list(E2)];
timestamp_to_list(N) when is_integer(N) ->
    [<<1:1>>, height_to_list(N, 2)].

height_to_list(N, B) ->
    Pow = floor(math:pow(2, B)),
    case N < Pow of
        true -> [<<0:1, N:B>>];
        false -> [<<1:1>> | height_to_list(N - Pow, B + 1)]
    end.

pad_bits(Bits) ->
    case 8 - bit_size(Bits) rem 8 of
        8 -> Bits;
        Padding -> <<Bits/bits, 0:Padding>>
    end.

is_zeroes(Bits) ->
    Size = bit_size(Bits),
    <<0:Size>> =:= Bits.

bits_to_binary(L) when is_list(L) ->
    << (bits_to_binary(V))  || V <- L >>;
bits_to_binary(V) -> V.

timestamp_to_binary(E) ->
    pad_bits(bits_to_binary(timestamp_to_list(E))).

binary_to_timestamp(Bits) ->
    {Timestamp, Rest} = btt(Bits),
    case is_zeroes(Rest) of true -> Timestamp end.

btt(<<0:1, 0:2, Rest/bits>>) ->
    {E, Rest1} = btt(Rest),
    {{0, 0, E}, Rest1};
btt(<<0:1, 1:2, Rest/bits>>) ->
    {E, Rest1} = btt(Rest),
    {{0, E, 0}, Rest1};
btt(<<0:1, 2:2, Rest/bits>>) ->
    {E1, Rest1} = btt(Rest),
    {E2, Rest2} = btt(Rest1),
    {{0, E1, E2}, Rest2};
btt(<<0:1, 3:2, 0:1, 0:1, Rest/bits>>) ->
    {N, Rest1} = btt(Rest),
    {E, Rest2} = btt(Rest1),
    {{N, 0, E}, Rest2};
btt(<<0:1, 3:2, 0:1, 1:1, Rest/bits>>) ->
    {N, Rest1} = btt(Rest),
    {E, Rest2} = btt(Rest1),
    {{N, E, 0}, Rest2};
btt(<<0:1, 3:2, 1:1, Rest/bits>>) ->
    {N, Rest1} = btt(Rest),
    {E1, Rest2} = btt(Rest1),
    {E2, Rest3} = btt(Rest2),
    {{N, E1, E2}, Rest3};
btt(<<1:1, Rest/bits>>) -> binary_to_height(Rest).

binary_to_height(Bits) -> binary_to_height(Bits, 2).

binary_to_height(<<0:1, Rest/bits>>, B) ->
    <<N:B, Rest1/bits>> = Rest,
    {N, Rest1};
binary_to_height(<<1:1, Rest/bits>>, B) ->
    {N, Rest1} = binary_to_height(Rest, B + 1),
    {N + floor(math:pow(2, B)), Rest1}.
