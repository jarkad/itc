%%%-------------------------------------------------------------------
%% @doc itc top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(itc_sup).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(SERVER, ?MODULE).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%% sup_flags() = #{strategy => strategy(),         % optional
%%                 intensity => non_neg_integer(), % optional
%%                 period => pos_integer()}        % optional
%% child_spec() = #{id => child_id(),       % mandatory
%%                  start => mfargs(),      % mandatory
%%                  restart => restart(),   % optional
%%                  shutdown => shutdown(), % optional
%%                  type => worker(),       % optional
%%                  modules => modules()}   % optional
init([]) ->
    SupFlags = #{strategy => one_for_all},
    Children = [#{id => events_db,
                  start =>
                      {events_model,
                       start_link,
                       [{local, events}, "events.dets"]}}],
    {ok, {SupFlags, Children}}.

%% internal functions
