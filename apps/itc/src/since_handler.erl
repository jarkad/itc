-module(since_handler).

-export([init/2,
         content_types_provided/2,
         to_text/2,
         to_json/2,
         to_etf/2]).

-record(state, {}).

init(Req, []) -> {cowboy_rest, Req, #state{}}.

content_types_provided(Req, State) ->
    {[{<<"text/plain">>, to_text},
      {<<"application/json">>, to_json},
      {<<"application/x-etf">>, to_etf}],
     Req,
     State}.

to_etf(Req, State) ->
    Since = cowboy_req:binding(since, Req),
    {term_to_iovec(events_model:since(events, Since)),
     Req,
     State}.

to_text(Req, State) ->
    Since = cowboy_req:binding(since, Req),
    {io_lib:format("~tp",
                   [events_model:since(events, Since)]),
     Req,
     State}.

to_json(Req, State) ->
    Since = cowboy_req:binding(since, Req),
    {jsx:encode(lists:map(fun ({T, P}) ->
                                  #{timestamp =>
                                        base64:encode(itc_clock:timestamp_to_binary(T)),
                                    data => P}
                          end,
                          events_model:since(events, Since))),
     Req,
     State}.
