{
  inputs = {
    devshell.url = "github:numtide/devshell";
    nixpkgs.url = "nixpkgs/nixos-unstable";

    devshell.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {
    self,
    nixpkgs,
    devshell,
  }: let
    inherit (nixpkgs) lib;
    eachSystem = lib.genAttrs ["x86_64-linux" "aarch64-linux"];
    pkgsFor = eachSystem (system:
      import nixpkgs {
        inherit system;
        overlays = [devshell.overlays.default];
      });
    depsFor = eachSystem (system:
      (import ./replit.nix {
        pkgs = pkgsFor.${system};
      })
      .deps);
  in {
    devShells = eachSystem (system: let
      pkgs = pkgsFor.${system};
    in {
      default = pkgs.devshell.mkShell {
        imports = ["${devshell}/extra/locale.nix"];
        extra.locale = {
          lang = "en_US.UTF-8";
        };
        devshell = {
          name = "itc";
          packages = depsFor.${system};
        };
      };
    });

    formatter = eachSystem (system: pkgsFor.${system}.alejandra);

    packages = eachSystem (system: let
      pkgs = pkgsFor.${system};
      beamDeps = import ./rebar-deps.nix {inherit (pkgs) fetchHex fetchgit fetchFromGitHub;};
      checkouts =
        pkgs.linkFarm "itc-deps"
        (lib.mapAttrsToList (name: pkg: {
            name = "_checkouts/${name}";
            path = pkg;
          })
          beamDeps);
    in {
      default = pkgs.beamPackages.rebar3Relx {
        version = "0.1.0";
        pname = "itc";
        src = ./.;
        releaseType = "release";
        profile = "prod";
        inherit checkouts;
        buildPlugins = [pkgs.beamPackages.pc];
      };
    });
  };
}
