{pkgs}: {
  deps = [
    pkgs.erlang
    pkgs.gawk # needed to run release
    pkgs.rebar3
    pkgs.rnix-lsp
  ];
}
